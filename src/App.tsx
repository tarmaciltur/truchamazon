import { Component } from 'react';
import './css/App.css';
import ProductOverview from './components/ProductOverview';
import { Product } from './model/Product';
import ProductList from './components/ProductList';
import axios from 'axios';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Col, Container, Navbar, Row } from 'react-bootstrap';

interface IProps { }

interface IState {
  selected: number,
  products: Product[],
}

class App extends Component<IProps, IState> {
  constructor(props: IProps) {
    super(props)
    this.state = {
      selected: 0,
      products: []
    }
  }

  componentDidMount() {
    axios.get('http://localhost:8000/api/v1/products',).then((res) => {
      let products: Product[] = res.data.products;
      console.log(res.data)
      console.log(products);
      this.setState({
        selected: 0,
        products: products,
      })
    });
  }

  onProductSelected = (pos: number) => {
    this.setState(() => {
      return {
        selected: pos,
      }
    })
  }

  render() {
    let product = this.state.products[this.state.selected]
    let productOverview: JSX.Element = <div></div>
    if (product !== undefined) {
      productOverview = <ProductOverview product={product} />
    }
    return (
      <div className="App">
        <Container fluid className="noPadding" >
          <Row className="noMargin">
            <Col className="noPadding">
              <Navbar bg="dark" variant="dark">
                <Container className="justify-content-center">
                  <Navbar.Brand href="#home">
                    Truchamazon
                  </Navbar.Brand>
                </Container>
              </Navbar>
            </Col>
          </Row>
          <Row className="noMargin">
            <Col xs={4} md={2} className="noPadding">
              <ProductList products={this.state.products} selected={this.state.selected} onProductSelected={this.onProductSelected} />
            </Col>
            <Col xs={20} md={10}>
              {productOverview}
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default App;
