import { Component } from "react";
import { Product } from "../model/Product";
import { ListGroup } from "react-bootstrap";

interface IProps {
  selected: number,
  products: Product[],
  onProductSelected: (pos: number) => void,
}

interface IState {
  selected: number,
  products: Product[],
}

class ProductList extends Component<IProps, IState> {
  constructor(props: IProps) {
    super(props)
    console.log(props)
    this.state = {
      selected: 0,
      products: props.products,
    }
  }

  componentWillReceiveProps(newProps: IProps) {
    this.setState({
      selected: newProps.selected,
      products: newProps.products
    });
  }

  changeSelection(pos: number) {
    if (pos !== this.state.selected) {
      this.setState({ selected: pos });
      this.props.onProductSelected(pos);
    }
  }

  render() {
    console.log(this.state.products)
    let products = this.state.products.map((item, pos) => {
      const selected = pos === this.state.selected;
      return <ListGroup.Item active={selected} key={pos} onClick={() => { this.changeSelection(pos) }}>{item.title}</ListGroup.Item>
    });


    return (
      <ListGroup variant="flush">
        {products}
      </ListGroup>
    );
  }
}
export default ProductList;