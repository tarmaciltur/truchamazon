import { Component } from "react";
import Styles from "../css/ColorSelector.module.css"
import ColorSelectorItem from "./ColorSelectorItem"
import { ProductStyle } from "../model/Product";

interface IProps {
  colors: ProductStyle[],
  selected: number,
  onColorSelected: (pos: number) => void,
}

interface IState {
  selected: number,
}

class ColorSelector extends Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);

    this.state = {
      selected: props.selected
    }
  }

  componentWillReceiveProps(newProps: IProps) {
    this.setState({
      selected: newProps.selected
    });
  }

  SelectionChanged = (selected: number) => {
    this.setState({ selected: selected })
    this.props.onColorSelected(selected)
  }

  render() {
    let colorSelector: JSX.Element = <div></div>
    if (this.props.colors.length > 1) {
      let imagelist = this.props.colors.map((item, pos) => {
        const selected = pos === this.state.selected ? true : false;
        return (<ColorSelectorItem key={pos} pos={pos} image={item.image} name={item.name} selected={selected} onClick={this.SelectionChanged} />);
      });

      colorSelector =
        <div>
          <h2 className="sectionText">Select Color</h2>
          <div className={Styles.colorSelector}>
            {imagelist}
          </div>
        </div>;
    }

    return colorSelector;
  }
}
export default ColorSelector;