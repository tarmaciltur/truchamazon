import { Component } from "react";
import ColorSelector from "./ColorSelector";
import Styles from '../css/ProductOverview.module.css';
import FeatureSelector from "./FeatureSelector";
import Heart from "../heart.gif"
import { Product } from "../model/Product";
import { Button, Col, Container, Image, Row } from "react-bootstrap";

interface IProps {
  product: Product,
}

interface IState {
  image: number,
  feature: number,
  time: string,
}

class ProductOverview extends Component<IProps, IState> {
  constructor(props: IProps) {
    super(props)
    console.log(props)
    this.state = {
      image: 0,
      feature: 0,
      time: "00:00",
    }
  }

  componentWillReceiveProps(newProps: IProps) {
    this.setState({
      image: 0,
      feature: 0,
    });
  }

  componentDidMount() {
    let secondInterval = setInterval(() => {
      let date = new Date();
      let seconds = date.getSeconds();
      if (seconds === 0) {
        clearInterval(secondInterval);
        setInterval(this.changeTime, 60 * 1000);
      }
      this.changeTime();
    }, 1000);
  }

  changeTime = () => {
    let date = new Date();
    let minutes = date.getMinutes().toLocaleString("es-Ar", {
      minimumIntegerDigits: 2,
    });
    let hour = date.getHours().toLocaleString("es-Ar", {
      minimumIntegerDigits: 2,
    });
    if (hour + ":" + minutes !== this.state.time) {
      this.setState(
        {
          time: hour + ":" + minutes,
        }
      );
    }
  }

  onColorSelected = (pos: number) => {
    this.setState(
      {
        image: pos,
      }
    );
  }

  onFeatureSelected = (pos: number) => {
    this.setState(
      {
        feature: pos,
      }
    );
  }

  render() {
    let feature: JSX.Element = <div></div>;
    let featureSelector: JSX.Element = <div></div>;
    if (this.props.product.features !== undefined) {
      let heart = <div className={Styles.heartDiv}><img className={Styles.heartImage} src={Heart} alt="heart" /><div>60</div></div>
      let featureElement = this.state.feature === 0 ? <div>{this.state.time}</div> : heart;
      feature = <div className={Styles.featureDiv}>
        <div className={Styles.feature}>
          {featureElement}
        </div>
      </div>

      featureSelector = <FeatureSelector features={this.props.product.features} onFeatureSelected={this.onFeatureSelected} />
    }

    let colorSelector: JSX.Element = <div></div>;
    if (this.props.product.styles !== undefined) {
      colorSelector = <ColorSelector colors={this.props.product.styles} onColorSelected={this.onColorSelected} selected={this.state.image} />
    }

    return (<Container fluid className="mt-5">
      <Row>
        <Col>
          <div className={Styles.imageDiv}>
            <Image src={this.props.product.styles[this.state.image].image} alt="Product" fluid />
            {feature}
          </div>
        </Col>
        <Col>
          <h1>{this.props.product.title}</h1>
          <h4 className="mt-3">{this.props.product.description}</h4>
          {colorSelector}
          {featureSelector}
          <Button className="mt-5" size="lg">Buy Now</Button>
        </Col>
      </Row>
    </Container>
    )
  }
}
export default ProductOverview;