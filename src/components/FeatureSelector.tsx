import { Component } from "react";
import Styles from "../css/FeatureSelector.module.css"

interface IProps {
  features: string[],
  onFeatureSelected: (pos: number) => void,
}

interface IState {
  selected: number,
}

class FeatureSelector extends Component<IProps, IState> {
  constructor(props: IProps) {
    super(props)
    this.state = {
      selected: 0
    }
  }

  changeSelection = (pos: number) => {
    if (pos !== this.state.selected) {
      this.setState({ selected: pos })
      this.props.onFeatureSelected(pos)
    }
  }

  render() {
    let featurelist = this.props.features.map((item, pos) => {
      const selected = pos === this.state.selected ? Styles.selected : "";
      return (
        <button key={pos} className={`${Styles.feature} interactive ${selected}`} onClick={() => { this.changeSelection(pos) }}>
          {item}
        </button>
      );
    });

    return (
      <div>
        <h2 className="sectionText">Feature List</h2>
        <div className={Styles.featureSelector} >
          {featurelist}
        </div>
      </div>);
  }
}
export default FeatureSelector;