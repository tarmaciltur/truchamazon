import React from "react";
import Styles from "../css/ColorSelectorItem.module.css"

interface IProps {
  pos: number,
  image: string,
  name: string,
  selected: boolean
  onClick: (pos: number) => void,
}


const ColorSelectorItem = (props: IProps) => {
  const selected = props.selected ? Styles.selected : null;

  const onClick = () => {
    if (!selected) {
      props.onClick(props.pos)
    }
  }

  return (
    <div className={`${Styles.container} ${selected}`}>
      <img src={props.image} className={`${Styles.image} interactive`} alt={props.name} onClick={onClick} />
    </div>
  );
}
export default ColorSelectorItem;