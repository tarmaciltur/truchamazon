const ProductData = {
    id: "asd123",
    title: 'FitBit 19 - The Smartest Watch',
    description: 'Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor.',
    styles: [
        {
            name: 'Black Strap',
            image: 'https://imgur.com/iOeUBV7.png'
        },
        {
            name: 'Red Strap',
            image: 'https://imgur.com/PTgQlim.png'
        },
        {
            name: 'Blue Strap',
            image: 'https://imgur.com/Mplj1YR.png'
        },
        {
            name: 'Purple Strap',
            image: 'https://imgur.com/xSIK4M8.png'
        },
    ],
    features: [
        "Time", "Heart Rate"
    ]
}

Object.freeze(ProductData); //This line of code just makes your object as a constant. No values can be updated.

export default ProductData;