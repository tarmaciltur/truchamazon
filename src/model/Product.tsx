interface ProductStyle {
  name: string,
  image: string,

}

interface Product {
  id: string,
  title: string,
  description: string,
  styles: ProductStyle[],
  features: string[],
}

export type { Product, ProductStyle };